<?php

namespace rotor;

#[\Attribute]
class Get extends Route {
    public function __construct($route) {
        parent::__construct($route, ['GET']);
    }
}
